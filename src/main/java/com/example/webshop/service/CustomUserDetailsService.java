package com.example.webshop.service;

import com.example.webshop.domain.User;

public interface CustomUserDetailsService {

    User saveUser(User user);
}
