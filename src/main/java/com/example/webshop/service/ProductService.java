package com.example.webshop.service;

import com.example.webshop.domain.Product;

import java.util.List;

public interface ProductService {

    Product findProductById(Long id);

    List<Product> findAllProducts();

    Product saveProduct(Product product);

}
