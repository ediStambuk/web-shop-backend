package com.example.webshop.service;

import com.example.webshop.domain.Cart;

public interface CartService {

    Cart createCart();
}
