package com.example.webshop.service;

import com.example.webshop.domain.CartItem;
import com.example.webshop.repositories.CartItemRepository;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartItemServiceImpl implements CartItemService {

    private final CartItemRepository cartItemRepository;

    public CartItemServiceImpl(CartItemRepository cartItemRepository) {
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public List<CartItem> findAll() {
        return cartItemRepository.findAll();
    }

    @Override
    public List<CartItem> findCartItemsById(Long cartId) {
        CartItem example = new CartItem();
        example.setCartId(cartId);
        Example<CartItem> exampleCart = Example.of(example);
        return cartItemRepository.findAll(exampleCart);
    }

    @Override
    public CartItem createCartItem(CartItem cartItem) {
        return cartItemRepository.save(cartItem);
    }

    @Override
    public CartItem updateCartItemNumber(Long cartItemId, Integer numberOfProducts) {
        CartItem cartItem = cartItemRepository.findById(cartItemId).get();
        cartItem.setNumber(numberOfProducts);
        return cartItemRepository.save(cartItem);
    }
}
