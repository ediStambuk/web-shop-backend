package com.example.webshop.service;

import com.example.webshop.domain.CartItem;

import java.util.List;

public interface CartItemService {

    List<CartItem> findAll();

    List<CartItem> findCartItemsById(Long cartId);

    CartItem createCartItem(CartItem cartItem);

    CartItem updateCartItemNumber(Long cartItemId, Integer numberOfProducts);

}
