package com.example.webshop.service;

import com.example.webshop.Util.PasswordUtil;
import com.example.webshop.domain.Cart;
import com.example.webshop.domain.User;
import com.example.webshop.repositories.CartRepository;
import com.example.webshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService, CustomUserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private PasswordUtil passwordUtil;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

    @Override
    public User saveUser(User user) {
        Cart cart = new Cart();
        Cart cartWithId = cartRepository.save(cart);
        user.setCartId(cartWithId.getId());
        user.setPassword(passwordUtil.hashPassword(user.getPassword()));
        return userRepository.save(user);
    }
}
