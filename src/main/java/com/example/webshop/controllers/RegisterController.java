package com.example.webshop.controllers;

import com.example.webshop.domain.User;
import com.example.webshop.service.CustomUserDetailsService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(RegisterController.BASE_URL)
public class RegisterController {

    public static final String BASE_URL = "/api/register";

    private final CustomUserDetailsService customUserDetailsService;

    public RegisterController(CustomUserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user) {
        return customUserDetailsService.saveUser(user);
    }
}
