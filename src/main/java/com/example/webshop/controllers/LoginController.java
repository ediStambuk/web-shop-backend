package com.example.webshop.controllers;

import com.example.webshop.Util.JwtUtil;
import com.example.webshop.Util.PasswordUtil;
import com.example.webshop.domain.auth.AuthRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(LoginController.BASE_URL)
public class LoginController {

    public static final String BASE_URL = "/api/login";

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private PasswordUtil passwordUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping
    public String generateToken(@RequestBody AuthRequest authRequest) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authRequest.getUserName(),
                        passwordUtil.hashPassword(authRequest.getPassword())));

        return jwtUtil.generateToken(authRequest.getUserName());
    }
}
