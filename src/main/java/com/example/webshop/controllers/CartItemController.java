package com.example.webshop.controllers;

import com.example.webshop.domain.CartItem;
import com.example.webshop.service.CartItemService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(CartItemController.BASE_URL)
public class CartItemController {

    public static final String BASE_URL = "/api/cartItem";

    private final CartItemService cartItemService;

    public CartItemController(CartItemService cartItemService) {
        this.cartItemService = cartItemService;
    }

    @GetMapping
    public List<CartItem> findAll() {
        return cartItemService.findAll();
    }

    @GetMapping("/{cartId}")
    public List<CartItem> findCartItemsById(@PathVariable Long cartId) {
        return cartItemService.findCartItemsById(cartId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartItem createCartItem(@RequestBody CartItem cartItem) {
        return cartItemService.createCartItem(cartItem);
    }

    @PutMapping("/{cartItemId}/{numberOfProducts}")
    public CartItem updateCartItemNumber(@PathVariable Long cartItemId, @PathVariable Integer numberOfProducts) {
        return cartItemService.updateCartItemNumber(cartItemId, numberOfProducts);
    }

}
