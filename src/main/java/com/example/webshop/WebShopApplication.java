package com.example.webshop;

import com.example.webshop.domain.User;
import com.example.webshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class WebShopApplication {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void initUsers() {
        User u1 = new User();
        u1.setUsername("mirko");
        u1.setPassword("mirko");
        u1.setCartId(Long.valueOf(1));
        userRepository.save(u1);

        User u2 = new User();
        u2.setUsername("janko");
        u2.setPassword("janko");
        u2.setCartId(Long.valueOf(2));
        userRepository.save(u2);
    }

    public static void main(String[] args) {
        SpringApplication.run(WebShopApplication.class, args);
    }

}
