package com.example.webshop.bootstrap;

import com.example.webshop.domain.Cart;
import com.example.webshop.domain.CartItem;
import com.example.webshop.domain.Product;
import com.example.webshop.repositories.CartItemRepository;
import com.example.webshop.repositories.CartRepository;
import com.example.webshop.repositories.ProductRepository;
import com.example.webshop.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {

    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;
    private final UserRepository userRepository;

    public BootStrapData(ProductRepository productRepository, CartRepository cartRepository,
                         CartItemRepository cartItemRepository, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Product p1 = new Product();
        p1.setTitle("Ormar");
        p1.setDescription("Jako dobar");
        p1.setPrice(Float.valueOf(1500));
        productRepository.save(p1);

        Product p2 = new Product();
        p2.setTitle("Stolica");
        p2.setDescription("Vrlo dobra");
        p2.setPrice(Float.valueOf(1800));
        productRepository.save(p2);

        Cart c = new Cart();
        cartRepository.save(c);

        Cart c2 = new Cart();
        cartRepository.save(c2);

        CartItem cartItem = new CartItem();
        cartItem.setNumber(2);
        cartItem.setProductId(Long.valueOf(1));
        cartItem.setCartId(Long.valueOf(1));
        cartItemRepository.save(cartItem);
    }
}
